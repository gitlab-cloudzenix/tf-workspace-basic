locals {
  instance_types = {
    dev   = "t2.micro"
    stage = "t2.small"
    prod  = "t2.medium"
  }
}
resource "aws_instance" "example" {
  ami           = "ami-033b95fb8079dc481"
  instance_type = local.instance_types[terraform.workspace]
  tags = {
    Name = "example-server-${terraform.workspace}"
  }
}
